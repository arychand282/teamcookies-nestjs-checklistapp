import { mockActivityEntity } from './mock.activityEntity';

export const mockPaginationResponse = {
  _metadata: {
    page: 0,
    totalRecord: 1,
    sizePerPage: 5,
    totalPage: 1,
    pages: [
      {
        current: 0,
      },
      { first: 0 },
      {
        last: 0,
      },
    ],
  },
  records: [{ id: mockActivityEntity.id, name: mockActivityEntity.name, isCompleted: mockActivityEntity.isCompleted }],
};
