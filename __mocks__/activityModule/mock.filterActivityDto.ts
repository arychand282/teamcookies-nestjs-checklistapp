import { FilterActivityDto } from '../../src/modules/activity/dto/request';

export const mockFilterActivityDto: FilterActivityDto = {
  page: 0,
  size: 5,
  sort: 'ASC',
  sortField: 'name',
  name: 'Buy',
};
