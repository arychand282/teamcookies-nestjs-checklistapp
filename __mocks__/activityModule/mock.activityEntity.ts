export const mockActivityEntity: any = {
  id: 'id-111',
  name: 'name-111',
  isCompleted: false,
  agent: {
    id: 'agentId-111',
  },
  createdAt: new Date(),
  createdBy: 'agentId-111',
  updatedAt: new Date(),
  updatedBy: null,
};
