<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo-small.svg" width="200" alt="Nest Logo" /></a>
</p>

[circleci-image]: https://img.shields.io/circleci/build/github/nestjs/nest/master?token=abc123def456
[circleci-url]: https://circleci.com/gh/nestjs/nest

  <p align="center">A progressive <a href="http://nodejs.org" target="_blank">Node.js</a> framework for building efficient and scalable server-side applications.</p>
    <p align="center">
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/v/@nestjs/core.svg" alt="NPM Version" /></a>
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/l/@nestjs/core.svg" alt="Package License" /></a>
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/dm/@nestjs/common.svg" alt="NPM Downloads" /></a>
<a href="https://circleci.com/gh/nestjs/nest" target="_blank"><img src="https://img.shields.io/circleci/build/github/nestjs/nest/master" alt="CircleCI" /></a>
<a href="https://coveralls.io/github/nestjs/nest?branch=master" target="_blank"><img src="https://coveralls.io/repos/github/nestjs/nest/badge.svg?branch=master#9" alt="Coverage" /></a>
<a href="https://discord.gg/G7Qnnhy" target="_blank"><img src="https://img.shields.io/badge/discord-online-brightgreen.svg" alt="Discord"/></a>
<a href="https://opencollective.com/nest#backer" target="_blank"><img src="https://opencollective.com/nest/backers/badge.svg" alt="Backers on Open Collective" /></a>
<a href="https://opencollective.com/nest#sponsor" target="_blank"><img src="https://opencollective.com/nest/sponsors/badge.svg" alt="Sponsors on Open Collective" /></a>
  <a href="https://paypal.me/kamilmysliwiec" target="_blank"><img src="https://img.shields.io/badge/Donate-PayPal-ff3f59.svg"/></a>
    <a href="https://opencollective.com/nest#sponsor"  target="_blank"><img src="https://img.shields.io/badge/Support%20us-Open%20Collective-41B883.svg" alt="Support us"></a>
  <a href="https://twitter.com/nestframework" target="_blank"><img src="https://img.shields.io/twitter/follow/nestframework.svg?style=social&label=Follow"></a>
</p>
  <!--[![Backers on Open Collective](https://opencollective.com/nest/backers/badge.svg)](https://opencollective.com/nest#backer)
  [![Sponsors on Open Collective](https://opencollective.com/nest/sponsors/badge.svg)](https://opencollective.com/nest#sponsor)-->

## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Prerequisites

- This project using nodejs v16
- This project using mysql for the database

## Installation

```bash
$ yarn
```

## Create .env file

```bash
$ touch .env
```

## Set .env file content

You can set .env file content by below values, but make sure you change the mysql database connection and credentials based on your own mysql database:
```
APP_PORT=3000
DATABASE_HOST=localhost
DATABASE_PORT=3306
DATABASE_USERNAME=root
DATABASE_PASSWORD=root
DATABASE_NAME=teamcookies_assessment
SECRET_KEY="ItIsASecretBetweenUs"
TOKEN_EXPIRES_IN=2h
```

## Running the app

```bash
# development
$ yarn start

# watch mode
$ yarn start:dev

# production mode
$ yarn start:prod
```

## Test

```bash
# unit tests
$ yarn test

# test coverage
$ yarn test:cov
```

## Testing the App

I attached a postman collection for this project. You can import it into Postman.

Below are the steps how to test the app.

- Open Postman and import a postman collection from this repo.
- Hit API request `Add New Agent (Agents/Add New Agent)` to create new Agent.
```
Body:
{
    "username": "codercupu",
    "firstName": "Coder",
    "lastName": "Cupu",
    "password": "P@ssw0rd"
}

Response:
{
    "id": "acbeffd2-ac2f-4b68-a6ae-0704c92c4ed7",
    "username": "codercupu",
    "firstName": "Coder",
    "lastName": "Cupu",
    "createdAt": "2022-12-27T13:12:20.347Z"
}
```

- Hit API `Login (Auth / login)` to get an Access Token using username and password you just created.
```
Body:
{
    "username": "codercupu",
    "password": "P@ssw0rd"
}

Response:
{
    "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI4MGUzMTQ1ZS0wMTE3LTRjYmYtODVjYy02YTA1NGUwM2NkMGEiLCJ1c2VybmFtZSI6ImNvZGVyY3VwdSIsImZpcnN0TmFtZSI6IkNvZGVyIiwibGFzdE5hbWUiOiJDdXB1IiwiaWF0IjoxNjcyMTQ2ODQxLCJleHAiOjE2NzIxNTQwNDF9.dgRPTkkJX4QbYAMaX8A7G9ynrBXgEP7HTNtJI8Y_tv0"
}
```

- Hit API `Add New Activity (Activities/Add New Activity)` to create new activity. Don't forget to set Authorization value in headers request by access token you just got when logged in.
```
Headers:
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI4MGUzMTQ1ZS0wMTE3LTRjYmYtODVjYy02YTA1NGUwM2NkMGEiLCJ1c2VybmFtZSI6ImNvZGVyY3VwdSIsImZpcnN0TmFtZSI6IkNvZGVyIiwibGFzdE5hbWUiOiJDdXB1IiwiaWF0IjoxNjcyMTQ1MjY4LCJleHAiOjE2NzIxNTI0Njh9.eOAJv9LTjAp1KzzQ0nUQdtUuqJ8zcfoHxmq5IC-xoXs

Body:
{
    "name": "Sleep Before 10"
}

Response:
{
    "id": "c4bf760a-3169-4dbe-869d-39978acefdaf",
    "name": "Sleep Before 10",
    "isCompleted": false
}
```

## Support

Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

## Stay in touch

- Author - [Kamil Myśliwiec](https://kamilmysliwiec.com)
- Website - [https://nestjs.com](https://nestjs.com/)
- Twitter - [@nestframework](https://twitter.com/nestframework)

## License

Nest is [MIT licensed](LICENSE).
