import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { mockActivityEntity, mockFilterActivityDto, mockPaginationResponse, MockRepository } from '../../../__mocks__';
import { ActivityService } from './activity.service';
import { CreateActivityDto, FilterActivityDto } from './dto/request';
import { ActivityEntity } from './entities/activity.entity';

describe('ActivityService', () => {
  let module: TestingModule;
  let activityService: ActivityService;
  let mockActivityRepo: Repository<ActivityEntity>;

  beforeEach(async () => {
    module = await Test.createTestingModule({
      providers: [
        ActivityService,
        {
          provide: getRepositoryToken(ActivityEntity),
          useValue: new MockRepository<ActivityEntity>(),
        },
      ],
    }).compile();

    activityService = module.get<ActivityService>(ActivityService);
    mockActivityRepo = await module.get<Repository<ActivityEntity>>(getRepositoryToken(ActivityEntity));

    jest.spyOn(console, 'error').mockImplementation(() => null);
  });

  afterEach(async () => {
    jest.clearAllMocks();
  });

  afterAll(async () => {
    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(activityService).toBeDefined();
    expect(mockActivityRepo).toBeDefined();
  });

  describe('create', () => {
    const mockCreateActivityDto: CreateActivityDto = {
      name: 'Buy Something',
    };
    const mockCreatedBy = 'anyone';

    it('should be success', async () => {
      const spyRepoSave = jest.spyOn(mockActivityRepo, 'save').mockResolvedValue({ ...mockActivityEntity });

      const result = await activityService.create({ ...mockCreateActivityDto }, mockCreatedBy);

      expect(result).toEqual({ ...mockActivityEntity });
      expect(spyRepoSave).toHaveBeenCalledWith({
        ...mockCreateActivityDto,
        createdBy: mockCreatedBy,
        agent: {
          id: mockCreatedBy,
        },
      });
    });

    it('should throw error', async () => {
      const spyRepoSave = jest
        .spyOn(mockActivityRepo, 'save')
        .mockRejectedValue(new Error('Got error when saving record'));

      try {
        await activityService.create({ ...mockCreateActivityDto }, mockCreatedBy);
      } catch (err) {
        expect(err).toBeDefined();
        expect(err.message).toEqual('Something went wrong');
        expect(spyRepoSave).toHaveBeenCalled();
      }
    });
  });

  describe('searchByAgentId', () => {
    it('should be success', async () => {
      const spyRepoFindAndCount = jest
        .spyOn(mockActivityRepo, 'findAndCount')
        .mockResolvedValue([[{ ...mockActivityEntity }], 1]);
      const result = await activityService.searchByAgentId('agentId-111', { ...mockFilterActivityDto });
      expect(result).toEqual({ ...mockPaginationResponse });
      expect(spyRepoFindAndCount).toHaveBeenCalled();
    });
    it('should throw error', async () => {
      const spyRepoFindAndCount = jest
        .spyOn(mockActivityRepo, 'findAndCount')
        .mockRejectedValue(new Error('Got error when retrieving records'));
      try {
        await activityService.searchByAgentId('agentId-111', { ...mockFilterActivityDto });
      } catch (err) {
        expect(err).toBeDefined();
        expect(err.message).toEqual('Something went wrong');
        expect(spyRepoFindAndCount).toHaveBeenCalled();
      }
    });
  });
});
