import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Like, Repository } from 'typeorm';
import { CreateActivityDto, FilterActivityDto, UpdateActivityDto } from './dto/request';
import { ActivitiesPaginationDto } from './dto/response';
import { ActivityEntity } from './entities/activity.entity';

@Injectable()
export class ActivityService {
  constructor(
    @InjectRepository(ActivityEntity)
    private activityRepository: Repository<ActivityEntity>
  ) {}

  async create(createActivityDto: CreateActivityDto, createdBy: string): Promise<ActivityEntity> {
    try {
      return await this.activityRepository.save({
        ...createActivityDto,
        agent: {
          id: createdBy,
        },
        createdBy,
      });
    } catch (err) {
      console.error(err);
      throw new BadRequestException(`Something went wrong`, err.message);
    }
  }

  async searchByAgentId(agentId: string, filterActivityDto: FilterActivityDto): Promise<ActivitiesPaginationDto> {
    try {
      const { page, size, sort, sortField, name } = filterActivityDto;
      const [activities, total] = await this.activityRepository.findAndCount({
        skip: page * size,
        take: size,
        where: {
          agent: {
            id: agentId,
          },
          name: name ? Like(`%${name}%`) : null,
        },
        order: {
          [sortField]: sort,
        },
      });

      return this.constructPaginationResponse(page, total, size, activities);
    } catch (err) {
      console.error(err);
      throw new BadRequestException(`Something went wrong`, err.message);
    }
  }

  async findOne(id: string): Promise<ActivityEntity> {
    try {
      return await this.activityRepository.findOneBy({
        id,
      });
    } catch (err) {
      console.error(err);
      throw new BadRequestException(`Something went wrong`, err.message);
    }
  }

  async update(id: string, updateActivityDto: UpdateActivityDto, updatedBy: string): Promise<ActivityEntity> {
    try {
      const { name } = updateActivityDto;
      const activity = await this.activityRepository.findOneBy({
        id,
      });
      if (!activity) {
        throw new BadRequestException(`Activity Doesn't Exist`, id);
      }

      activity.name = name;
      activity.updatedBy = updatedBy;
      return await this.activityRepository.save(activity);
    } catch (err) {
      console.error(err);
      throw new BadRequestException(`Something went wrong`, err.message);
    }
  }

  async updateCompletedStatus(id: string, isCompleted: boolean, updatedBy: string): Promise<ActivityEntity> {
    try {
      const activity = await this.activityRepository.findOneBy({
        id,
      });
      if (!activity) {
        throw new BadRequestException(`Activity Doesn't Exist`, id);
      }

      activity.isCompleted = isCompleted;
      activity.updatedBy = updatedBy;
      return await this.activityRepository.save(activity);
    } catch (err) {
      console.error(err);
      throw new BadRequestException(`Something went wrong`, err.message);
    }
  }

  async delete(id: string): Promise<void> {
    try {
      await this.activityRepository.delete({
        id,
      });
    } catch (err) {
      console.error(err);
      throw new BadRequestException(`Something went wrong`, err.message);
    }
  }

  private constructPaginationResponse(
    page: number,
    total: number,
    size: number,
    activities: ActivityEntity[]
  ): ActivitiesPaginationDto {
    const lastPage = Math.ceil(total / size) - 1;
    const pages: any[] = [
      {
        current: page,
      },
      {
        first: 0,
      },
    ];
    if (page > 0) {
      pages.push({
        previous: page - 1,
      });
    }
    if (page < lastPage) {
      pages.push({
        next: page + 1,
      });
    }
    pages.push({ last: lastPage });

    const activitiesPaginationDto: ActivitiesPaginationDto = {
      _metadata: {
        page,
        totalRecord: total,
        sizePerPage: size,
        totalPage: Math.ceil(total / size),
        pages,
      },
      records: activities.map((activity) => {
        const { id, name, isCompleted } = activity;
        return {
          id,
          name,
          isCompleted,
        };
      }),
    };

    return activitiesPaginationDto;
  }
}
