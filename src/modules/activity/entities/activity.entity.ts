import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { AgentEntity } from '../../agent/entities/agent.entity';

@Entity('activities')
export class ActivityEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ nullable: false })
  name: string;

  @Column({ default: false })
  isCompleted: boolean;

  @ManyToOne(() => AgentEntity, (agent) => agent.id, {
    eager: false,
  })
  @JoinColumn({ name: 'agentId' })
  agent: AgentEntity;

  @CreateDateColumn()
  createdAt: Date;

  @Column({ nullable: false })
  createdBy: string;

  @UpdateDateColumn()
  updatedAt: Date;

  @Column({ nullable: true, default: null })
  updatedBy?: string;
}
