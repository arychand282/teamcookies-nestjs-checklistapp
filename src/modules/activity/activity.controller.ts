import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards, Req, Query } from '@nestjs/common';
import { JwtAuthGuard } from '../auth/jwtAuth.guard';
import { ActivityService } from './activity.service';
import { CreateActivityDto, FilterActivityDto, UpdateActivityDto, UpdateActivityStatusDto } from './dto/request';
import { ActivityDto, ActivitiesPaginationDto } from './dto/response';
import { ActivityEntity } from './entities/activity.entity';

@UseGuards(JwtAuthGuard)
@Controller('api/v1/activities')
export class ActivityController {
  constructor(private readonly activityService: ActivityService) {}

  @Post('/')
  async create(@Req() req: any, @Body() createActivityDto: CreateActivityDto): Promise<CreateActivityDto> {
    const createdBy = req.user.id;
    return this.toActivityDto(await this.activityService.create(createActivityDto, createdBy));
  }

  @Get('/agent/:agentId')
  async searchByAgentId(
    @Param('agentId') agentId: string,
    @Query('page') page: string,
    @Query('size') size?: string,
    @Query('sort') sort?: string,
    @Query('sortField') sortField?: string,
    @Query('name') name?: string
  ): Promise<ActivitiesPaginationDto> {
    const filterActivityDto: FilterActivityDto = {
      page: parseInt(page) || 0,
      size: parseInt(size) || 10,
      sort: sort || 'DESC',
      sortField: sortField || 'createdAt',
      name,
    };
    return await this.activityService.searchByAgentId(agentId, filterActivityDto);
  }

  @Get('/:id')
  async findOne(@Param('id') id: string): Promise<ActivityDto> {
    return this.toActivityDto(await this.activityService.findOne(id));
  }

  @Patch('/:id')
  async update(
    @Req() req: any,
    @Param('id') id: string,
    @Body() updateActivityDto: UpdateActivityDto
  ): Promise<ActivityDto> {
    const updatedBy = req.user.id;
    return this.toActivityDto(await this.activityService.update(id, updateActivityDto, updatedBy));
  }

  @Patch('/update_status/:id')
  async updateStatus(
    @Req() req: any,
    @Param('id') id: string,
    @Body() updateActivityStatusDto: UpdateActivityStatusDto
  ): Promise<ActivityDto> {
    const updatedBy = req.user.id;
    return this.toActivityDto(
      await this.activityService.updateCompletedStatus(id, updateActivityStatusDto.isCompleted, updatedBy)
    );
  }

  @Delete('/:id')
  async remove(@Param('id') id: string): Promise<void> {
    await this.activityService.delete(id);
  }

  private toActivityDto(activityEntity: ActivityEntity): ActivityDto {
    const { id, name, isCompleted } = activityEntity;

    return {
      id,
      name,
      isCompleted,
    };
  }
}
