import { Test, TestingModule } from '@nestjs/testing';
import { mockActivityEntity, mockFilterActivityDto, mockPaginationResponse } from '../../../__mocks__';
import { ActivityController } from './activity.controller';
import { ActivityService } from './activity.service';
import { CreateActivityDto } from './dto/request';
import { ActivityDto } from './dto/response';

const mockResultDecodedJwt = {
  user: {
    id: 'agentId-111',
    username: 'agentnickname',
  },
};

class MockActivityService {
  static create() {
    return null;
  }
  static searchByAgentId() {
    return null;
  }
}

describe('ActivityController', () => {
  let activityController: ActivityController;
  let activityService: ActivityService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ActivityController,
        {
          provide: ActivityService,
          useValue: MockActivityService,
        },
      ],
    }).compile();

    activityController = module.get<ActivityController>(ActivityController);
    activityService = module.get<ActivityService>(ActivityService);
  });

  afterAll(async () => {
    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(activityController).toBeDefined();
    expect(activityService).toBeDefined();
  });

  describe('create', () => {
    const mockBodyRequest: CreateActivityDto = {
      name: 'Buy something',
    };

    const mockResult: ActivityDto = {
      id: mockActivityEntity.id,
      name: mockActivityEntity.name,
      isCompleted: mockActivityEntity.isCompleted,
    };

    it('should be success', async () => {
      const spyActivityServiceCreate = jest
        .spyOn(activityService, 'create')
        .mockResolvedValue({ ...mockActivityEntity });
      const result = await activityController.create({ ...mockResultDecodedJwt }, { ...mockBodyRequest });

      expect(result).toEqual(mockResult);
      expect(spyActivityServiceCreate).toHaveBeenCalled();
    });
    it('should throw error', async () => {
      const spyActivityServiceCreate = jest
        .spyOn(activityService, 'create')
        .mockRejectedValue(new Error('Got some error'));
      try {
        await activityController.create({ ...mockResultDecodedJwt }, { ...mockBodyRequest });
      } catch (err) {
        expect(err).toBeDefined();
        expect(err.message).toEqual('Got some error');
        expect(spyActivityServiceCreate).toHaveBeenCalled();
      }
    });
  });

  describe('searchByAgentId', () => {
    it('should be success', async () => {
      const spyActivityServiceSearchByAgentId = jest
        .spyOn(activityService, 'searchByAgentId')
        .mockResolvedValue({ ...mockPaginationResponse });
      const result = await activityController.searchByAgentId('agentId-111', '0', '5', 'ASC', 'name', 'Buy');

      expect(result).toEqual({ ...mockPaginationResponse });
      expect(spyActivityServiceSearchByAgentId).toHaveBeenCalled();
    });
    it('should throw error', async () => {
      const spyActivityServiceSearchByAgentId = jest
        .spyOn(activityService, 'searchByAgentId')
        .mockRejectedValue(new Error('Got some error'));
      try {
        await activityController.searchByAgentId('agentId-111', '0', '5', 'ASC', 'name', 'Buy');
      } catch (err) {
        expect(err).toBeDefined();
        expect(err.message).toEqual('Got some error');
        expect(spyActivityServiceSearchByAgentId).toHaveBeenCalled();
      }
    });
  });
});
