import { PaginationMetadata } from '../../../../utils/dto';
import { ActivityDto } from './activity.dto';

export class ActivitiesPaginationDto {
  _metadata: PaginationMetadata;
  records: ActivityDto[];
}
