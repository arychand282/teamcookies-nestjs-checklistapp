import { IsBoolean, IsNotEmpty, IsString } from 'class-validator';

export class ActivityDto {
  @IsNotEmpty()
  @IsString()
  id: string;

  @IsNotEmpty()
  @IsString()
  name: string;

  @IsBoolean()
  isCompleted: boolean;
}
