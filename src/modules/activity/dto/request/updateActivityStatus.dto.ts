import { IsBoolean } from 'class-validator';

export class UpdateActivityStatusDto {
  @IsBoolean()
  isCompleted: boolean;
}
