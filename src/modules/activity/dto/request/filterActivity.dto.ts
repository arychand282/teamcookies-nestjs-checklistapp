export class FilterActivityDto {
  page: number;

  size: number;

  sort: string;

  sortField: string;

  name?: string;
}
