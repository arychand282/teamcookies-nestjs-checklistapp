import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcryptjs';
import { AgentService } from '../agent/agent.service';
import { AgentEntity } from '../agent/entities/agent.entity';

@Injectable()
export class AuthService {
  constructor(
    private agentService: AgentService,
    private jwtService: JwtService,
  ) {}

  async validateAgentCredential(
    username: string,
    plainPassword: string,
  ): Promise<AgentEntity | undefined> {
    const agent = await this.agentService.findByUsername(username);

    const isValidated = agent
      ? await bcrypt.compare(plainPassword, agent.password)
      : false;

    return isValidated ? agent : null;
  }

  async login(agent: any): Promise<any> {
    const { id, username, firstName, lastName } = agent;
    return {
      access_token: this.jwtService.sign({
        sub: id,
        username,
        firstName,
        lastName,
      }),
    };
  }
}
