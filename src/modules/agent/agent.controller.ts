import { Body, Controller, Post } from '@nestjs/common';
import { AgentService } from './agent.service';
import { CreateAgentDto } from './dto/request';
import { AgentDto } from './dto/response';
import { AgentEntity } from './entities/agent.entity';

@Controller('api/v1/agents')
export class AgentController {
  constructor(private readonly agentService: AgentService) {}

  @Post('/')
  async create(@Body() createAgentDto: CreateAgentDto): Promise<AgentDto> {
    return this.toAgentDto(await this.agentService.create(createAgentDto));
  }

  private toAgentDto(agentEntity: AgentEntity): AgentDto {
    const { id, username, firstName, lastName, createdAt } = agentEntity;
    return {
      id,
      username,
      firstName,
      lastName,
      createdAt,
    };
  }
}
