import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcryptjs';
import { CreateAgentDto } from './dto/request';
import { AgentEntity } from './entities/agent.entity';

@Injectable()
export class AgentService {
  constructor(
    @InjectRepository(AgentEntity)
    private agentRepository: Repository<AgentEntity>,
  ) {}

  async create(createAgentDto: CreateAgentDto): Promise<AgentEntity> {
    try {
      const salt = await bcrypt.genSalt();
      const hashedPassword = await bcrypt.hash(createAgentDto.password, salt);

      return await this.agentRepository.save({
        ...createAgentDto,
        password: hashedPassword,
      });
    } catch (err) {
      console.error(err);
      throw new BadRequestException(err.message);
    }
  }

  async findByUsername(username: string): Promise<AgentEntity> {
    return await this.agentRepository.findOneBy({
      username,
    });
  }
}
