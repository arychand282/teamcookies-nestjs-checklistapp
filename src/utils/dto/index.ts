export class PaginationMetadata {
  page: number;
  totalRecord: number;
  sizePerPage: number;
  totalPage: number;
  pages: any[];
}
