import { MigrationInterface, QueryRunner } from 'typeorm';

export class createActivitiesTable1671782683768 implements MigrationInterface {
  name = 'createActivitiesTable1671782683768';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE \`activities\` (\`id\` varchar(36) NOT NULL, \`name\` varchar(255) NOT NULL, \`completed\` tinyint NOT NULL DEFAULT 0, \`createdAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`createdBy\` varchar(255) NOT NULL, \`updatedAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`updatedBy\` varchar(255) NULL, \`agentId\` varchar(36) NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`
    );
    await queryRunner.query(
      `ALTER TABLE \`activities\` ADD CONSTRAINT \`FK_b12671dc6bd64eed25eeabb2cd7\` FOREIGN KEY (\`agentId\`) REFERENCES \`agents\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE \`activities\` DROP FOREIGN KEY \`FK_b12671dc6bd64eed25eeabb2cd7\``);
    await queryRunner.query(`DROP TABLE \`activities\``);
  }
}
