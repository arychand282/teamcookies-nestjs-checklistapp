import { MigrationInterface, QueryRunner } from 'typeorm';

export class alterActivitiesTableRenameCompletedField1671785960385 implements MigrationInterface {
  name = 'alterActivitiesTableRenameCompletedField1671785960385';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`activities\` CHANGE \`completed\` \`isCompleted\` tinyint NOT NULL DEFAULT '0'`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`activities\` CHANGE \`isCompleted\` \`completed\` tinyint NOT NULL DEFAULT '0'`
    );
  }
}
