import { MigrationInterface, QueryRunner } from 'typeorm';

export class createAgentsTable1671779776214 implements MigrationInterface {
  name = 'createAgentsTable1671779776214';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE \`agents\` (\`id\` varchar(36) NOT NULL, \`username\` varchar(255) NOT NULL, \`firstName\` varchar(255) NOT NULL, \`lastName\` varchar(255) NULL, \`password\` varchar(255) NOT NULL, \`createdAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), UNIQUE INDEX \`IDX_661196c4c60fb3a2b93feff6af\` (\`username\`), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP INDEX \`IDX_661196c4c60fb3a2b93feff6af\` ON \`agents\``);
    await queryRunner.query(`DROP TABLE \`agents\``);
  }
}
